﻿using Onion.Domain.SeedWork;

namespace Onion.Domain.AggregatesModel.OrderAggregate
{
	//This is just the RepositoryContracts or Interface defined at the Domain Layer
	//as requisite for the Order Aggregate
	public interface IOrderRepository : IRepository<Order>
	{
	}
}
