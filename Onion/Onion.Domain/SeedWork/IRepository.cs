﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Onion.Domain.SeedWork
{
	public interface IRepository<Entity> : IDisposable where Entity : BaseEntity
	{
		Task<IEnumerable<Entity>> Get(long startRowIndex, long maximumRows);
		Task AddRange(List<Entity> item);
		Task Add(Entity item);
		Task DeleteRange(List<Entity> items);
		Task<Entity> Find(long id);
		Task Remove(Entity item);
		Task UpdateRange(List<Entity> items);
		Task Update(Entity item);
		Task SaveChanges();
		void RollbackChanges();
	}
}
