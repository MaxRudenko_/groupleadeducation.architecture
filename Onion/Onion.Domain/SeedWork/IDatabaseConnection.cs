﻿using System.Data;

namespace Onion.Domain.SeedWork
{
	public interface IDatabaseConnection : IDbConnection
	{
		IDbTransaction Transaction { get; set; }
	}
}
