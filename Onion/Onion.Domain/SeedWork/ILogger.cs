﻿using System;

namespace Onion.Domain.SeedWork
{
	public interface ILogger
	{
		void LogTrace(string message);
		void LogCritical(string message, Exception exception);
	}
}
