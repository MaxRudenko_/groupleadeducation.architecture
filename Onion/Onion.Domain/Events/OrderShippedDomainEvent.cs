﻿using MediatR;
using Onion.Domain.AggregatesModel.OrderAggregate;

namespace Onion.Domain.Events
{
	public class OrderShippedDomainEvent : INotification
	{
		public Order Order { get; }

		public OrderShippedDomainEvent(Order order)
		{
			Order = order;
		}
	}
}
