﻿using Dapper;
using Dapper.Contrib.Extensions;
using MediatR;
using Onion.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Onion.Infrastracture.Repositories
{
    public abstract class BaseRepository<Entity> : IRepository<Entity> where Entity : BaseEntity
    {
        private List<BaseEntity> _changeTracker;
        private readonly IMediator _mediator;
        protected readonly string _tableName;
        public IDatabaseConnection Connection { get; }

        public BaseRepository(string tableName, IDatabaseConnection dbConnection, IMediator mediator, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            _changeTracker = new List<BaseEntity>();
            _tableName = tableName;
            Connection = dbConnection;
            _mediator = mediator;
            if (Connection.State == ConnectionState.Closed)
            {
                Connection.Open();
            }
            if (Connection.Transaction == null)
            {
                Connection.BeginTransaction(isolationLevel);
            }
        }

        private async Task DispatchDomainEventsAsync(IMediator mediator)
        {
            var domainEntities = _changeTracker.Where(x => x.DomainEvents != null && x.DomainEvents.Any());

            var domainEvents = domainEntities
                .SelectMany(x => x.DomainEvents)
                .ToList();

            domainEntities.ToList()
                .ForEach(entity => entity.ClearDomainEvents());

            var tasks = domainEvents
                .Select(async (domainEvent) =>
                {
                    await mediator.Publish(domainEvent);
                });

            await Task.WhenAll(tasks);
        }

        public async Task SaveChanges()
        {
            if (Connection.Transaction == null)
            {
                throw new ArgumentNullException("Transaction has already been commited.");
            }
            try
            {
                await DispatchDomainEventsAsync(_mediator);
                Connection.Transaction.Commit();
            }
            catch (Exception)
            {
                Connection.Transaction.Rollback();
                throw;
            }
            finally
            {
                Connection.Transaction = null;
            }
        }

        public void RollbackChanges()
        {
            if (Connection.Transaction != null)
            {
                Connection.Transaction.Rollback();
            }
        }

        public virtual async Task Add(Entity item)
        {
            _changeTracker.Add(item);
            var id = await Connection.InsertAsync(item, Connection.Transaction);
            item.SetId(id);
        }

        public virtual async Task AddRange(List<Entity> items)
        {
            _changeTracker.AddRange(items);
            await Connection.InsertAsync(items, Connection.Transaction);
        }

        public virtual async Task UpdateRange(List<Entity> items)
        {
            _changeTracker.AddRange(items);
            await Connection.UpdateAsync(items, Connection.Transaction);
        }

        public virtual async Task Update(Entity item)
        {
            _changeTracker.Add(item);
            await Connection.UpdateAsync(item, Connection.Transaction);
        }

        public async Task DeleteRange(List<Entity> entities)
        {
            if (entities.Count == 0)
            {
                return;
            }
            _changeTracker.AddRange(entities);
            await Connection.DeleteAsync(entities, Connection.Transaction);
        }

        public virtual async Task Remove(Entity item)
        {
            _changeTracker.Add(item);
            await Connection.ExecuteAsync("DELETE FROM " + _tableName + " WHERE Id=@Id", new { Id = item.Id }, Connection.Transaction);
        }

        public virtual async Task<Entity> Find(long id)
        {
            var result = await Connection.QueryFirstAsync<Entity>("SELECT TOP(1) * FROM " + _tableName + " WHERE Id=@Id",
                new { Id = id });
            return result;
        }

        public virtual async Task<IEnumerable<Entity>> Get(long startRowIndex, long maximumRows)
        {
            var result = await Connection.QueryAsync<Entity>(
                $@"DECLARE @first_id nvarchar(50)
				SET ROWCOUNT @startRowIndex
				SELECT @first_id = Id FROM {_tableName} ORDER BY Id
				SET ROWCOUNT @maximumRows
				SELECT *
				FROM {_tableName}
				WHERE Id >= @first_id
				ORDER BY Id",
                new
                {
                    startRowIndex,
                    maximumRows
                });
            return result;
        }

        public void Dispose()
        {
            Connection.Transaction = null;
        }
    }
}
