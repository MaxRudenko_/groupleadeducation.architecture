﻿using MediatR;
using Onion.Domain.AggregatesModel.OrderAggregate;
using Onion.Domain.SeedWork;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Onion.Application.CommandHandlers.OrderCommandHandlers
{
    public class CreateOrderCommandHandler : IRequestHandler<CreateOrderCommand, bool>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly ILogger _logger;
        private readonly IMediator _mediator;

        // Using DI to inject infrastructure persistence Repositories
        public CreateOrderCommandHandler(IMediator mediator, IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        public async Task<bool> Handle(CreateOrderCommand message, CancellationToken cancellationToken)
        {
            try
            {
                //// Add Integration event to clean the basket
                //var orderStartedIntegrationEvent = new OrderStartedIntegrationEvent(message.UserId);
                //await _orderingIntegrationEventService.AddAndSaveEventAsync(orderStartedIntegrationEvent);

                // Add/Update the Buyer AggregateRoot
                // DDD patterns comment: Add child entities and value-objects through the Order Aggregate-Root
                // methods and constructor so validations, invariants and business logic 
                // make sure that consistency is preserved across the whole aggregate
                var address = new Address(message.Street, message.City, message.State, message.Country, message.ZipCode);
                var order = new Order(message.UserId, message.UserName, address, message.CardTypeId, message.CardNumber, message.CardSecurityNumber, message.CardHolderName, message.CardExpiration);

                foreach (var item in message.OrderItems)
                {
                    order.AddOrderItem(item.ProductId, item.ProductName, item.UnitPrice, item.Discount, item.PictureUrl, item.Units);
                }
                await _orderRepository.Add(order);
                await _orderRepository.SaveChanges();
                return true;
            }
            catch (Exception exception)
            {
                _logger.LogCritical(nameof(CreateOrderCommandHandler), exception);
                return false;
            }
        }
    }
}
