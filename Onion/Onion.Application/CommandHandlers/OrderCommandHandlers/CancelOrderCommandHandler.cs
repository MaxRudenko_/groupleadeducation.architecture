﻿using MediatR;
using Onion.Domain.AggregatesModel.OrderAggregate;
using Onion.Domain.SeedWork;
using System;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;

namespace Onion.Application.Commands
{
    public class CancelOrderCommand : IRequest<bool>
    {
        [DataMember]
        public int OrderNumber { get; private set; }

        public CancelOrderCommand(int orderNumber)
        {
            OrderNumber = orderNumber;
        }
    }

    public class CancelOrderCommandHandler : IRequestHandler<CancelOrderCommand, bool>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly ILogger _logger;

        public CancelOrderCommandHandler(IOrderRepository orderRepository,ILogger logger)
        {
            _logger = logger;
            _orderRepository = orderRepository;
        }

        public async Task<bool> Handle(CancelOrderCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var orderToUpdate = await _orderRepository.Find(command.OrderNumber);
                if (orderToUpdate == null)
                {
                    return false;
                }

                orderToUpdate.SetCancelledStatus();
                await _orderRepository.SaveChanges();
                return true;
            }
            catch (Exception exception)
            {
                _logger.LogCritical(nameof(CancelOrderCommandHandler), exception);
                return false;
            }
        }
    }
}
