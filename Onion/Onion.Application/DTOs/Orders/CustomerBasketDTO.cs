﻿using System.Collections.Generic;

namespace Onion.Application.DTOs
{
    public class CustomerBasketDTO
    {
        public string BuyerId { get; set; }
        public List<BasketDTOItem> Items { get; set; }

        public CustomerBasketDTO(string customerId)
        {
            BuyerId = customerId;
            Items = new List<BasketDTOItem>();
        }
    }
}
