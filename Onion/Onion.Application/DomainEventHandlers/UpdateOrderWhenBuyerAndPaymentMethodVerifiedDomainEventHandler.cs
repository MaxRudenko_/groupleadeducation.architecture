﻿using MediatR;
using Onion.Domain.AggregatesModel.OrderAggregate;
using Onion.Domain.Events;
using Onion.Domain.SeedWork;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Onion.Application.DomainEventHandlers
{
	public class UpdateOrderWhenBuyerAndPaymentMethodVerifiedDomainEventHandler : INotificationHandler<BuyerAndPaymentMethodVerifiedDomainEvent>
	{
		private readonly IOrderRepository _orderRepository;
		private readonly ILogger _logger;

		public UpdateOrderWhenBuyerAndPaymentMethodVerifiedDomainEventHandler(IOrderRepository orderRepository, ILogger logger)
		{
			_orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
			_logger = logger ?? throw new ArgumentNullException(nameof(logger));
		}

		// Domain Logic comment:
		// When the Buyer and Buyer's payment method have been created or verified that they existed, 
		// then we can update the original Order with the BuyerId and PaymentId (foreign keys)
		public async Task Handle(BuyerAndPaymentMethodVerifiedDomainEvent buyerPaymentMethodVerifiedEvent, CancellationToken cancellationToken)
		{
			var orderToUpdate = await _orderRepository.Find(buyerPaymentMethodVerifiedEvent.OrderId);
			orderToUpdate.SetBuyerId(buyerPaymentMethodVerifiedEvent.Buyer.Id);
			orderToUpdate.SetPaymentId(buyerPaymentMethodVerifiedEvent.Payment.Id);
			await _orderRepository.Update(orderToUpdate);
			_logger.LogTrace($"Order with Id: {buyerPaymentMethodVerifiedEvent.OrderId} has been successfully updated with a payment method id: { buyerPaymentMethodVerifiedEvent.Payment.Id }");
		}
	}
}
