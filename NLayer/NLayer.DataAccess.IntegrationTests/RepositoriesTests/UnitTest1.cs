using NLayer.DataAccess.Entities;
using NLayer.DataAccess.Repositories;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Xunit;

namespace NLayer.DataAccess.IntegrationTests
{
	public class UnitTest1
	{
		public UnitTest1()
		{
			
		}

		[Fact]
		public async Task Test1()
		{
			try
			{
				var connectionString = @"Data Source=195.26.92.83,1433;Initial Catalog=A1; Integrated Security=false; User ID=test; Password=qwe123";
				var user = new User()
				{
					Email = "Email",
					LastName = "LastName",
					//Name = "Name",
					Phone = "Phone",
					Role = UserRole.User
				};
				var userProfile = new UserProfile()
				{
					Address = "Address",
					ImageLink = "ImageLink"
				};
				using (var sqlConnection = new DatabaseConnection(new SqlConnection(connectionString)))
				{
					var userProfileRepository = new UserProfileRepository(sqlConnection);
					var userRepository = new UserRepository(sqlConnection);
					long id = 1;
					using (var transactionScope = userProfileRepository.BeginTransaction())
					{
						id = await userRepository.Add(user);
						await userProfileRepository.Add(userProfile);
						transactionScope.Commit();
					}
					var userTemp = await userRepository.Find(id);
					await userRepository.Add(user);
				}
			}catch(Exception exception)
			{

			}
		}
	}
}
