﻿using NLayer.DataAccess.Entities;
using NLayer.DataAccess.Repositories.Interfaces;
using NLayer.Services.ViewModels.UserAdministration;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NLayer.Services.Services
{
	public class UserAdministrationService
	{
		private IUserProfileRepository _userProfileRepository;
		private IUserRepository _userRepository;

		public UserAdministrationService(IUserProfileRepository userProfileRepository, IUserRepository userRepository)
		{
			_userProfileRepository = userProfileRepository;
			_userRepository = userRepository;
		}

		public async Task Create(CreateUserAdministrationViewModel model)
		{
			User user = model.MapTo();
			UserProfile userProfile = model.Profile.MapTo();
			using (var transactionScope = _userProfileRepository.BeginTransaction())
			{
				var isExist = await _userRepository.Any(model.Email);
				if (isExist)
				{
					transactionScope.Rollback();
					throw new ValidationException("User with same email already exist!");
				}
				user.UserProfileId = await _userProfileRepository.Add(userProfile);
				await _userRepository.Add(user);
				transactionScope.Commit();
			}
		}

		public async Task Update(UpdateUserAdministrationViewModel model)
		{
			using (var transactionScope = _userProfileRepository.BeginTransaction())
			{
				var user = await _userRepository.Find(model.Id);

				var userProfile = await _userProfileRepository.Find(model.Profile.Id);
				await _userRepository.Update(user);
				await _userProfileRepository.Update(userProfile);
				transactionScope.Commit();
			}
		}

		public async Task<IEnumerable<UserAdministrationViewModel>> Get(long startRowIndex, long maximumRows)
		{
			var users = await _userRepository.Get(startRowIndex, maximumRows);
			return users.Select(user => new UserAdministrationViewModel(user)).ToList();
		}

		public async Task RemoveUser(long id)
		{
			using (var transactionScope = _userProfileRepository.BeginTransaction())
			{
				User user = await _userRepository.Find(id);
				if (user == null)
				{
					throw new ValidationException("No user with such id!");
				}
				await _userRepository.Remove(user);
				await _userProfileRepository.Remove(user.UserProfileId);
			}
		}

	}
}