﻿using Microsoft.Extensions.DependencyInjection;

namespace NLayer.Services
{
	public static class Startup
	{
		public static void Configure(string connectionString, IServiceCollection serviceCollection)
		{
			DataAccess.Startup.EnsureUpdate(connectionString);
			DataAccess.Startup.RegisterDependencies(connectionString, serviceCollection);
		}
	}
}
