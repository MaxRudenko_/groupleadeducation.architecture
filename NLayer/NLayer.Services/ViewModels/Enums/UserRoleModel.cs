﻿namespace NLayer.Services.Models.Enums
{
	public enum UserRoleModel
	{
		User = 1,
		Moderator = 2,
		Admin = 3
	}
}
