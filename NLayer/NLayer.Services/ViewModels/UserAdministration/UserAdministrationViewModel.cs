﻿using NLayer.DataAccess.Entities;
using NLayer.Services.Models.Enums;

namespace NLayer.Services.ViewModels.UserAdministration
{
	public class UserAdministrationViewModel
	{
		public long Id { get; set; }
		public string Email { get; set; }
		public string Name { get; set; }
		public string LastName { get; set; }
		public string Phone { get; set; }
		public UserRoleModel Role { get; set; }

		public UserAdministrationViewModel()
		{

		}

		public UserAdministrationViewModel(User user)
		{
			Id = user.Id;
			Email = user.Email;
			Name = user.Name;
			LastName = user.LastName;
			Phone = user.Phone;
			Role = (UserRoleModel)user.Role;
		}
	}
}
