﻿using NLayer.DataAccess.Entities;
using NLayer.Services.Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace NLayer.Services.ViewModels.UserAdministration
{
	public class UpdateUserAdministrationViewModel
	{
		[Required]
		public long Id { get; set; }
		[EmailAddress]
		[Required]
		public string Email { get; set; }
		[Required]
		[StringLength(10)]
		public string Name { get; set; }
		[Required]
		[StringLength(10)]
		public string LastName { get; set; }
		[Required]
		[Phone]
		public string Phone { get; set; }
		[Required]
		public UserRoleModel Role { get; set; }
		public ProfileUpdateUserAdministrationViewModelItem Profile { get; set; }

		public void MapTo(User user)
		{
			user.Email = Email;
			user.Name = Name;
			user.LastName = LastName;
			user.Phone = Phone;
			user.Role = (UserRole)Role;
		}
	}

	public class ProfileUpdateUserAdministrationViewModelItem
	{
		[Required]
		public long Id { get; set; }
		[Required]
		public string Address { get; set; }
		[Required]
		public string ImageLink { get; set; }
		public UserProfile MapTo(UserProfile userProfile)
		{
			userProfile.Address = Address;
			userProfile.ImageLink = ImageLink;
			return userProfile;
		}
	}
}
