﻿using NLayer.DataAccess.Entities;
using NLayer.Services.Models.Enums;

namespace NLayer.Services.ViewModels.UserAdministration
{
	public class CreateUserAdministrationViewModel
	{
		public string Email { get; set; }
		public string Name { get; set; }
		public string LastName { get; set; }
		public string Phone { get; set; }
		public UserRoleModel Role { get; set; }
		public ProfileAddUserAdministrationViewModelItem Profile { get; set; }

		public User MapTo()
		{
			return new User()
			{
				Email = Email,
				LastName = LastName,
				Name = Name,
				Phone = Phone,
				Role = (UserRole)Role
			};
		}
	}

	public class ProfileAddUserAdministrationViewModelItem
	{
		public string Address { get; set; }
		public string ImageLink { get; set; }

		public UserProfile MapTo()
		{
			return new UserProfile()
			{
				Address = Address,
				ImageLink = ImageLink
			};
		}
	}
}
