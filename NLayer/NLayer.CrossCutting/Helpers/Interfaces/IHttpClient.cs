﻿using System.Net.Http;
using System.Threading.Tasks;

namespace NLayer.CrossCutting.Helpers.Interfaces
{
	public interface IHttpClient
	{
		Task<HttpResponseMessage> PostAsync(string requestUri, HttpContent content);
		Task<HttpResponseMessage> GetAsync(string requestUri);
	}
}
