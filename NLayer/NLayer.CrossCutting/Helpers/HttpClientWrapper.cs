﻿using NLayer.CrossCutting.Helpers.Interfaces;
using System.Net.Http;
using System.Threading.Tasks;

namespace NLayer.CrossCutting.Helpers
{
	public class HttpClientWrapper : IHttpClient
	{
		private HttpClient _httpClient;

		public HttpClientWrapper()
		{
			_httpClient = new HttpClient();
		}

		public async Task<HttpResponseMessage> PostAsync(string requestUri, HttpContent content)
		{
			return await _httpClient.PostAsync(requestUri, content);
		}
		
		public async Task<HttpResponseMessage> GetAsync(string requestUri)
		{
			return await _httpClient.GetAsync(requestUri);
		}
	}
}
