﻿using Dapper;
using Dapper.Contrib.Extensions;
using NLayer.DataAccess.Entities;
using NLayer.DataAccess.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace NLayer.DataAccess.Repositories
{
	public abstract class BaseRepository<Entity> : IBaseRepository<Entity> where Entity : BaseEntity
	{
		private IDatabaseConnection _connection;
		protected readonly string _tableName;

		public IDatabaseConnection Connection
		{
			get
			{
				return _connection;
			}
		}

		public BaseRepository(string tableName, IDatabaseConnection dbConnection)
		{
			_tableName = tableName;
			_connection = dbConnection;
		}

		public IBaseRepository<Entity> BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified)
		{
			_connection.Open();
			_connection.BeginTransaction(isolationLevel);
			return this;
		}

		public virtual void Commit()
		{
			if (_connection.Transaction == null)
			{
				throw new InvalidOperationException("Transaction has already been commited.");
			}
			try
			{
				_connection.Transaction.Commit();
			}
			catch (Exception)
			{
				_connection.Transaction.Rollback();
				throw;
			}
			_connection.Transaction = null;
		}

		public virtual void Rollback()
		{
			if (_connection.Transaction != null)
			{
				_connection.Transaction.Rollback();
			}
		}

		public virtual async Task<long> Add(Entity item)
		{
			return await Connection.InsertAsync(item, _connection.Transaction);
		}

		public virtual async Task AddRange(List<Entity> item)
		{
			await Connection.InsertAsync(item, _connection.Transaction);
		}

		public virtual async Task UpdateRange(List<Entity> items)
		{
			await Connection.UpdateAsync(items, _connection.Transaction);
		}

		public virtual async Task Update(Entity item)
		{
			await Connection.UpdateAsync(item, _connection.Transaction);
		}

		public async Task DeleteRange(List<Entity> entities)
		{
			if (entities.Count == 0)
			{
				return;
			}
			await Connection.DeleteAsync(entities, _connection.Transaction);
		}

		public virtual async Task Remove(Entity item)
		{
			await Connection.ExecuteAsync("DELETE FROM " + _tableName + " WHERE Id=@Id", new { Id = item.Id }, _connection.Transaction);
		}

		public virtual async Task Remove(long id)
		{
			await Connection.ExecuteAsync("DELETE FROM " + _tableName + " WHERE Id=@Id", new { Id = id }, _connection.Transaction);
		}

		public virtual async Task<Entity> Find(long id)
		{
			var result = await Connection.QueryFirstAsync<Entity>("SELECT TOP(1) * FROM " + _tableName + " WHERE Id=@Id",
				new { Id = id }, _connection.Transaction);
			return result;
		}

		public virtual async Task<IEnumerable<Entity>> Get(long startRowIndex, long maximumRows)
		{
			var result = await Connection.QueryAsync<Entity>(
				$@"DECLARE @first_id nvarchar(50)
				SET ROWCOUNT @startRowIndex
				SELECT @first_id = Id FROM {_tableName} ORDER BY Id
				SET ROWCOUNT @maximumRows
				SELECT *
				FROM {_tableName}
				WHERE Id >= @first_id
				ORDER BY Id",
				new {
					startRowIndex,
					maximumRows
				}, _connection.Transaction);
			return result;
		}

		public void Dispose()
		{
			_connection.Transaction = null;
		}
	}
}
