﻿using NLayer.DataAccess.Entities;
using NLayer.DataAccess.Repositories.Interfaces;
using System;
using System.Data;
using System.Threading.Tasks;

namespace NLayer.DataAccess.Repositories
{
	public class UserRepository : BaseRepository<User>, IUserRepository
	{
		public UserRepository(IDatabaseConnection dbConnection) : base("Users", dbConnection)
		{

		}

		public Task<bool> Any(string email)
		{
			throw new NotImplementedException();
		}
	}
}
