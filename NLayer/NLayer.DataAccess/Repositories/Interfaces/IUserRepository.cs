﻿using NLayer.DataAccess.Entities;
using System.Threading.Tasks;

namespace NLayer.DataAccess.Repositories.Interfaces
{
	public interface IUserRepository : IBaseRepository<User>
	{
		Task<bool> Any(string email);
	}
}
