﻿using NLayer.DataAccess.Entities;

namespace NLayer.DataAccess.Repositories.Interfaces
{
	public interface IUserProfileRepository : IBaseRepository<UserProfile>
	{
		
	}
}
