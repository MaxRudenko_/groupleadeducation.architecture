﻿using NLayer.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace NLayer.DataAccess.Repositories.Interfaces
{
	public interface IBaseRepository<Entity> : IDisposable where Entity : IBaseEntity
	{
		Task<IEnumerable<Entity>> Get(long startRowIndex, long maximumRows);
		Task AddRange(List<Entity> item);
		Task<long> Add(Entity item);
		Task DeleteRange(List<Entity> items);
		Task<Entity> Find(long id);
		Task Remove(Entity item);
		Task Remove(long id);
		Task UpdateRange(List<Entity> items);
		Task Update(Entity item);
		IBaseRepository<Entity> BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified);
		void Rollback();
		void Commit();
	}
}
