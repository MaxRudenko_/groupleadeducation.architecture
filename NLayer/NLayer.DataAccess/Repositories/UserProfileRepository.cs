﻿using NLayer.DataAccess.Entities;
using NLayer.DataAccess.Repositories.Interfaces;

namespace NLayer.DataAccess.Repositories
{
	public class UserProfileRepository : BaseRepository<UserProfile>, IUserProfileRepository
	{
		public UserProfileRepository(IDatabaseConnection dbConnection) : base("UserProfiles", dbConnection)
		{

		}
	}
}
