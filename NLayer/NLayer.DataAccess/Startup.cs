﻿using DbUp;
using DbUp.Engine;
using Microsoft.Extensions.DependencyInjection;
using NLayer.DataAccess.Repositories;
using NLayer.DataAccess.Repositories.Interfaces;
using System;
using System.Data.SqlClient;
using System.Reflection;

namespace NLayer.DataAccess
{
	public class Startup
	{
		public static void RegisterDependencies(string connectionString, IServiceCollection serviceCollection)
		{
			serviceCollection.AddScoped<IDatabaseConnection>((context)=>new DatabaseConnection(new SqlConnection(connectionString)));
			serviceCollection.AddScoped<IUserProfileRepository, UserProfileRepository>();
			serviceCollection.AddScoped<IUserRepository, UserRepository>();
		}

		public static DatabaseUpgradeResult EnsureUpdate(string connectionString)
		{
			var upgrader = DeployChanges.To
				.SqlDatabase(connectionString)
				.JournalToSqlTable("dbo", "Migrations")
				.WithScriptsAndCodeEmbeddedInAssembly(
					Assembly.GetExecutingAssembly(),
					(string s) => s.Contains($".Scripts."))
				.WithTransactionPerScript()
				.LogToConsole()
				.WithExecutionTimeout(new TimeSpan(0, 0, 90))
				.Build();
			var result = upgrader.PerformUpgrade();
			if (!result.Successful)
			{
				throw new System.Exception(result.Error.Message);
			}
			return result;
		}
	}
}
