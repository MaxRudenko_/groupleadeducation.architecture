﻿
using Dapper.Contrib.Extensions;

namespace NLayer.DataAccess.Entities
{
	public class User : BaseEntity
	{
		public string Email { get; set; }
		public string LastName { get; set; }
		public string Name { get; set; }
		public string Phone { get; set; }
		public UserRole Role { get; set; }
		public long UserProfileId { get; set; }
		[Computed]
		public UserProfile UserProfile { get; set; }
	}

	public enum UserRole
	{
		User = 1,
		Moderator = 2,
		Admin = 3
	}
}
