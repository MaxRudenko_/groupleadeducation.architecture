﻿using System;

namespace NLayer.DataAccess.Entities
{
	public interface IBaseEntity
	{
		DateTime CreationDate { get; set; }
		long Id { get; set; }
	}
}