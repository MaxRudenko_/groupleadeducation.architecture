﻿namespace NLayer.DataAccess.Entities
{
	public class UserProfile : BaseEntity
	{
		public string Address { get; set; }
		public string ImageLink { get; set; }
	}
}
