﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NLayer.DataAccess.Entities
{
	public abstract class BaseEntity : IBaseEntity
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; set; }
		
		[Required]
		public DateTime CreationDate
		{
			get;
			set;
		}

		protected BaseEntity()
		{
			DateTime now = DateTime.UtcNow;
			CreationDate = new DateTime(now.Ticks / 100000 * 100000, now.Kind);
		}
	}
}
