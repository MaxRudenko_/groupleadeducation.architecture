﻿using System.Data;

namespace NLayer.DataAccess
{
	public interface IDatabaseConnection : IDbConnection
	{
		IDbTransaction Transaction { get; set; }
	}
}