﻿using System.Data;

namespace NLayer.DataAccess
{
	/// <summary>
	/// Decorator for the connection class, exposing additional info like it's transaction.
	/// </summary>
	public class DatabaseConnection : IDatabaseConnection
	{
		private IDbConnection connection = null;
		private IDbTransaction transaction = null;

		public IDbConnection Connection
		{
			get { return connection; }
		}

		public IDbTransaction Transaction
		{
			get { return transaction; }
			set { transaction = value; }
		}

		public DatabaseConnection(IDbConnection connection)
		{
			this.connection = connection;
		}

		public IDbTransaction BeginTransaction(IsolationLevel il)
		{
			transaction = connection.BeginTransaction(il);
			return transaction;
		}

		public IDbTransaction BeginTransaction()
		{
			transaction = connection.BeginTransaction();
			return transaction;
		}

		public void ChangeDatabase(string databaseName)
		{
			connection.ChangeDatabase(databaseName);
		}

		public void Close()
		{
			connection.Close();
		}

		public string ConnectionString
		{
			get
			{
				return connection.ConnectionString;
			}
			set
			{
				connection.ConnectionString = value;
			}
		}

		public int ConnectionTimeout
		{
			get { return connection.ConnectionTimeout; }
		}

		public IDbCommand CreateCommand()
		{
			return connection.CreateCommand();
		}

		public string Database
		{
			get { return connection.Database; }
		}

		public void Open()
		{
			connection.Open();
		}

		public ConnectionState State
		{
			get { return connection.State; }
		}
		
		public void Dispose()
		{
			connection.Dispose();
		}
	}
}
